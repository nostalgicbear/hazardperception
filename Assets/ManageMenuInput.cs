﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using System;
using UnityEngine.SceneManagement;

public class ManageMenuInput : MonoBehaviour {

    public TMPro.TMP_InputField usernameField;
    public TMPro.TMP_InputField passwordField;
    private TMPro.TMP_InputField activeField;
    public static ManageMenuInput Instance = null;


    public GameObject go1, go2, go3;
    bool loginSuccessful = false;
	// Use this for initialization
	void Awake () {
        if (Instance == null)
        {
            Instance = this;
        }
        else {

            Destroy(gameObject);
        }
	}

    private void Start()
    {
        activeField = usernameField;
    }

    public void ChangeActiveInputField(TMPro.TMP_InputField newActiveInputField)
    {
        activeField = newActiveInputField;
    }

    public void AddLetterToActiveField(string letter)
    {
        activeField.text += letter;
    }

    public void RemoveLastLetter()
    {
        if(activeField.text.Length > 0)
        {
            string value = activeField.text;
            value = value.Substring(0, value.Length - 1);
            activeField.text = value;
        }
        
    }

    void Update()
    {
        if (loginSuccessful)
        {
            loginSuccessful = false;
            Destroy(go1);
            Destroy(go2);
            Destroy(go3);
            SceneManager.LoadScene("MainMenu_Gaze", LoadSceneMode.Additive);
        }
    }

    public void TryLogin()
    {
        Task.Run(VerifyLoginAgainstAzure);
    }

    public async Task VerifyLoginAgainstAzure()
    {
        try
        {
            var table = AzureMobileServiceClient.Client.GetTable<UserLogins>();

            await ReturnUserLoginsList(table);
        } catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    private async Task<List<UserLogins>> ReturnUserLoginsList(IMobileServiceTable<UserLogins> table)
    {
        var allEntries = await table.ToListAsync();
        //foreach (var entry in allEntries)
        //{
        //    entry.Name = entry.Name.ToLower();
        //    entry.Password = entry.Password.ToLower();
        //}

        foreach (var item in allEntries)
        {
            if (item.Name.Trim().Equals(usernameField.text, StringComparison.InvariantCultureIgnoreCase))
            {
                if(item.Password.Trim().Equals(passwordField.text, StringComparison.InvariantCultureIgnoreCase))
                {
                    loginSuccessful = true;
                    GlobalLogin.Instance.SetLoggedInUser(item.Name);
                }
               
            }
        }

        return allEntries;
    }

}
