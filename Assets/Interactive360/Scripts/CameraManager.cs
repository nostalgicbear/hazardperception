﻿using UnityEngine;
using UnityEngine.UI;
namespace Interactive360
{

    public class CameraManager : MonoBehaviour
    {

        [Tooltip("Only needed for Gaze Based Interactions")]
        public GameObject m_cameraUI;

        public HotspotManager hotSpotManager;


        private void Awake()
        {
            //DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            checkForOtherCameras();

            if(Input.GetKey(KeyCode.UpArrow))
            {
                transform.Rotate(Vector3.right * Time.deltaTime * 25);
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                transform.Rotate(Vector3.left * Time.deltaTime * 25);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Rotate(Vector3.up * Time.deltaTime * 25);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Rotate( - Vector3.up * Time.deltaTime * 25);
            }


        }


        //This method will check to see if any additional cameras become active. If any cameras not tagged as "Main Camera" are found, 
        //we will turn them off to avoid having multiple cameras running at once.
        private void checkForOtherCameras()
        {
            if (Camera.allCamerasCount > 1)
                foreach (Camera c in Camera.allCameras)
                {
                    if (c.gameObject.CompareTag("MainCamera") == false)
                    {
                        c.gameObject.SetActive(false);
                    }
                }
        }

        public void Test()
        {
            //Turn on other hotspots
            hotSpotManager.m_hotSpotsInScene[1].GetComponent<Image>().enabled = true;
        }



        //method to turn camera UI on
        public void turnUIOn()
        {
            if (m_cameraUI)
            {
                m_cameraUI.SetActive(true);
            }
        }

        //method to turn camera UI off
        public void turnUIoff()
        {
            if (m_cameraUI)
            {
                m_cameraUI.SetActive(false);
            }

        }


    }
}
