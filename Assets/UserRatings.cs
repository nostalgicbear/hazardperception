﻿

public class UserRatings  {
    public string Username { get; set; }
    public string Scenario { get; set; }
    public string Score { get; set; }
    public int TimeInSeconds{ get; set; }
    public string Id { get; set; }
}
