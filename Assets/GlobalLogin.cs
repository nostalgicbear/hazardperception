﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.WindowsAzure.MobileServices;

public class GlobalLogin : MonoBehaviour {

    public string loggedInUser;
    public static GlobalLogin Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public void SetLoggedInUser(string userName)
    {
        loggedInUser = userName;
    }

    public string GetLoggedInUser()
    {
        return loggedInUser;
    }



}
