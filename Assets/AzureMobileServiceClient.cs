﻿using Microsoft.WindowsAzure.MobileServices;

public class AzureMobileServiceClient  {
    private const string backendURL = "https://hazardperception.azurewebsites.net";
    private static MobileServiceClient client;


    public static MobileServiceClient Client
    {
        get
        {
            if(client == null)
            {
                client = new MobileServiceClient(backendURL);
            }

            return client;
        }
    }

}
