﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
/// <summary>
/// This handles saving scores out to the databasse. This object is instanciated at the main menu scene. It works out the total score possible for a scene based on the number of 
/// hotspots presents. It also handles sending data to the specified google form.
/// </summary>
public class SaveManager : MonoBehaviour {

    public static SaveManager Instance = null;
    Interactive360.MenuManager menuManager;
    //URL for the google doc that the info will be sent to
    [Tooltip("The link to the google form you wish to write to")]
    public string BASE_URL = "https://docs.google.com/forms/d/e/1FAIpQLScdXkSpD4rULU-8sAeib9zRHplfgoTfg3XZQ648yl0oF4xO9g/formResponse"; //Form to send data to on Google Forms

    public string username = ""; //The person doing the scenario
    public string scenario = "";
    public string grade = "";
    bool dataSent = false;

    private float score = 100;
    public float incorrectScoreAdjustment;
    private int numOfAnswersPerQuestion = 3; // As of now, every question only has 3 answers associated
    private int numOfHotspots;
    private int correctAnswersFound;
    bool runTimer = true;

    private float time = 0.0f;
    private int secondsUntilHotspotsFound = 0;
    bool writtenToAzure = false;

    //public bool WrittenToAzure { get { return writtenToAzure; } set { writtenToAzure = value; if (writtenToAzure) { LoadMenuScene(); } } }

    IMobileServiceTable<UserRatings> userRatingAzureTable;

    private void LoadMenuScene()
    {
        Debug.Log("Loading menu as data is written to azure");
        SceneManager.LoadScene("MainMenu_Gaze", LoadSceneMode.Additive);
    }

    private void CacheTable()
    {
        userRatingAzureTable = AzureMobileServiceClient.Client.GetTable<UserRatings>();
    }



    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }

        Debug.Log("Save Manager awake");

    }

    // Use this for initialization
    void Start () {
        if(GlobalLogin.Instance == null)
        {
            Debug.Log("Global login instance is null");
        }
        if(GlobalLogin.Instance.GetLoggedInUser() == null)
        {
            Debug.Log("LoggedInUser is null");
        }
        username = GlobalLogin.Instance.GetLoggedInUser();
        menuManager = GameObject.Find("MainMenuManager").GetComponent<Interactive360.MenuManager>();
	}

    private void Update()
    {
        if(runTimer)
        {
            IncrementTimer();
        }
        
    }

    private void IncrementTimer()
    {
        time += Time.deltaTime;
        secondsUntilHotspotsFound = (int)time % 60;
    }

    private void ResetTimer()
    {
        time = 0.0f;
        secondsUntilHotspotsFound = 0;
        runTimer = true;
        writtenToAzure = false;
    }


    /// <summary>
    /// Calculates the scenario name, the grade, and calls the POST coroutine that will send the entry to the google form
    /// </summary>
    public void SendToDatabase()
    {
        if(!dataSent)
        {
            runTimer = false; //Stop calculating how long the user has been searching for the hotspots
            scenario = CalculateScenario();
            grade = CalculateScore();
            StartCoroutine(Post(username, scenario, grade));
            Task.Run(SendToAzure);
            dataSent = true;
        }
        
    }

    /// <summary>
    /// Sends the data to the Google form
    /// </summary>
    /// <param name="_username"></param>
    /// <param name="_scenario"></param>
    /// <param name="_grade"></param>
    /// <returns></returns>
    IEnumerator Post(string _username, string _scenario, string _grade)
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.488078284", _username);
        form.AddField("entry.175491347", _scenario);
        form.AddField("entry.637074537", _grade);
        form.AddField("entry.699834745", secondsUntilHotspotsFound);

        byte[] rawData = form.data;
        WWW WWW = new WWW(BASE_URL, rawData);
        Debug.Log("trying to write to Google sheet");
        yield return WWW;
    }

    string CalculateScore()
    {
        return score.ToString();
    }

    string CalculateScenario()
    {
        return SceneManager.GetActiveScene().name;
    }

    /// <summary>
    /// Adjusts the current score based on values passed in
    /// </summary>
    /// <param name="adjustment"></param>
    public void AdjustScore(float adjustment)
    {
        score += adjustment;
    }

    /// <summary>
    /// Helper method used to pick a random name. Only used when there was no Azure database.
    /// </summary>
    /// <returns></returns>
    private string PickRandomName()
    {
        List<string> names = new List<string>();
        names.Add("Stephen");
        names.Add("Ed");
        names.Add("Ish");
        names.Add("Huzzy");
        names.Add("Jay");

        string nameToUse = names[Random.Range(0, names.Count - 1)];
        return nameToUse;
    }

    public int TotalCorrectAnswersFound()
    {
        return correctAnswersFound;
    }

    public void IncreaseCorrectAnswerFound()
    {
        correctAnswersFound += 1;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        CalculateScenarioTotalScore();
        correctAnswersFound = 0;
        ResetTimer();

        ///Quick dirty check to set the override position of the menu so its accessible from the scene 
        if (SceneManager.GetActiveScene().name == "Hotspot Example_Photogrammetry")
        {
            menuManager.OverrideMenuPosition(SceneManager.GetActiveScene().name);
        }
        else {
            if(menuManager != null)
            {
                menuManager.ResetMenuScaleAndPosition();
            }
            
        }
        
    }

    /// <summary>
    /// Checks to see how many hotspots are in the scene. Creates a total score based on that.
    /// </summary>
    void CalculateScenarioTotalScore()
    {
        GameObject[] hotspots = GameObject.FindGameObjectsWithTag("Hotspot");

        numOfHotspots = hotspots.Length;

        float individualQuestionWorth = score / numOfHotspots;
        incorrectScoreAdjustment = (individualQuestionWorth / numOfAnswersPerQuestion) * -1;

        Debug.Log("Incorrect answer deducts : " + incorrectScoreAdjustment);
        Debug.Log("Num of hotspots in scene is " + numOfHotspots);
    }

    public int ReturnNumberOfHotspots()
    {
        return numOfHotspots;
    }



    //--------------------------------------EVERYTHING BELOW HERE IS RELATED TO SAVING TO AZURE RATHER THAN GOOGLE SHEETS---------------------------------


    private async Task SendToAzureDatabase(IMobileServiceTable<UserRatings> table)
    {
        await table.InsertAsync(new UserRatings { Username = username, Scenario = scenario, Score = grade, TimeInSeconds = secondsUntilHotspotsFound});
        
        //WrittenToAzure = true;
        //After values have been sent to the database, change back to menu
    }


    private async Task SendToAzure()
    {
        var table = AzureMobileServiceClient.Client.GetTable<UserRatings>();
        Debug.Log("Sending to Azure");
        await SendToAzureDatabase(table);
    }



}
