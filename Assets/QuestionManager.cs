﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour {

    public List<GameObject> answers;
    public GameObject correctAnswer;
    public Image background;

    private int correctAnswersFound = 0;
    public void ActivateObject()
    {
        GetComponent<Text>().enabled = true;
        background.enabled = true;
        for (int i = 0; i < answers.Count; i++)
        {
            answers[i].GetComponent<Image>().enabled = true;
            answers[i].GetComponentInChildren<Text>().enabled = true;
            answers[i].GetComponent<BoxCollider>().enabled = true;
        }

    }

    /// <summary>
    /// Determines if an answer is the correct answer to a given question. If it is the correct answer, the CorrectAnswer method is called. Otherwise the IncorrectAnswer
    /// method is called
    /// </summary>
    /// <param name="selectedAnswer">The UI gameobject that has been selected as the answer to the question</param>
    public void ValidateAnswer(GameObject selectedAnswer)
    {
        Debug.Log("Selected answer is " + selectedAnswer.name + "   \\  and correct answer is " + correctAnswer.name);
        if (selectedAnswer == correctAnswer)
        {
            CorrectAnswer(selectedAnswer);
        }
        else {
            IncorrectAnswer(selectedAnswer);
           // ResetAll();
        }
        CollapsesQuestion(selectedAnswer);
    }
	
    /// <summary>
    /// Sets all answer backgrounds to be white (default when unselected)
    /// </summary>
	public void ResetAll()
    {
        for(int i = 0; i < answers.Count; i++)
        {
            answers[i].GetComponent<Image>().color = Color.white;
        }
    }

    /// <summary>
    /// Takes in the selected answer and changes its colour to green. 
    /// </summary>
    /// <param name="selectedAnswer"></param>
    public void CorrectAnswer(GameObject selectedAnswer)
    {
        selectedAnswer.GetComponent<Button>().image.color = Color.green;
        SaveManager.Instance.IncreaseCorrectAnswerFound();
        CheckAllHotspotsAreFound();
    }

    /// <summary>
    /// Checks that the number of hotspots found against the total number of hotspots in the scene. If all are found, the users score is sent to the database
    /// </summary>
    void CheckAllHotspotsAreFound()
    {
        if(SaveManager.Instance.TotalCorrectAnswersFound() == SaveManager.Instance.ReturnNumberOfHotspots())
        {
            Debug.Log("Calling SendToDatabase");
            SaveManager.Instance.SendToDatabase();
        }
    }

    void CollapsesQuestion(GameObject selectedAnswer)
    {
        selectedAnswer.GetComponent<Collider>().enabled = false;
            if (selectedAnswer != correctAnswer)
            {
                StartCoroutine(HideAnswer(selectedAnswer.transform, false));
            }
            else {
            InstantlyDeleteIncorrectAnswers();
            StartCoroutine(MoveAnswerToCentre(correctAnswer.transform, true));
            }
        

        gameObject.transform.parent.GetComponent<Collider>().enabled = false;
    }

    void InstantlyDeleteIncorrectAnswers()
    {
        foreach(GameObject g in answers)
        {
            if(g != correctAnswer)
            {
                g.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Called when an incorrect answer is selected
    /// </summary>
    /// <param name="answer"></param>
    /// <param name="correctAnswerSelected"></param>
    /// <returns></returns>
    IEnumerator HideAnswer(Transform answer, bool correctAnswerSelected)
    {
        while (answer.localScale.y > 0.02f)
        {
            answer.localScale = new Vector3(answer.localScale.x, answer.localScale.y - 0.02f, answer.localScale.z);
            yield return new WaitForFixedUpdate();
        }
        if(!correctAnswerSelected)
        {
            answer.gameObject.SetActive(false);
        }
        
        yield return null;
    }

    /// <summary>
    /// Called when a correct answer is selected
    /// </summary>
    /// <param name="answer"></param>
    /// <param name="correctAnswerSelected"></param>
    /// <returns></returns>
    IEnumerator MoveAnswerToCentre(Transform answer, bool correctAnswerSelected)
    {
        float dist = CalculateDist(answer, answer.parent.transform.parent);
        while(dist > 0.05f)
        {
            dist = CalculateDist(answer, answer.parent.transform.parent);
            answer.position = Vector3.Lerp(answer.transform.position, answer.parent.transform.parent.transform.position, Time.deltaTime * 0.5f);
            yield return new WaitForFixedUpdate();
        }

        if (!correctAnswerSelected)
        {
            answer.gameObject.SetActive(false);
        }
        else {
            answer.transform.parent.gameObject.GetComponent<Text>().enabled = false;
            answer.transform.parent.transform.parent.GetChild(0).GetComponent<Image>().enabled = false;
        }
        yield return null; 
    }

    float CalculateDist(Transform answer, Transform questionPosition)
    {
        float dist = Vector3.Distance(answer.position, questionPosition.position);
        return dist;
    }


    /// <summary>
    /// Takes in the selected answer and changes its colour to green
    /// </summary>
    /// <param name="selectedAnswer"></param>
    public void IncorrectAnswer(GameObject selectedAnswer)
    {
        selectedAnswer.GetComponent<Button>().image.color = Color.red;
        SaveManager.Instance.AdjustScore(SaveManager.Instance.incorrectScoreAdjustment);
    }

}
