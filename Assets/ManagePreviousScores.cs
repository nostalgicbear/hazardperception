﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;
public class ManagePreviousScores : MonoBehaviour {

    public TMPro.TMP_Text previousScoresText;
    public TMPro.TMP_Text previousScoresTitleText;
    List<UserRatings> entriesForCurrentUser = new List<UserRatings>();
    bool scoresUpdated = false;
    // Use this for initialization
    void Start () {
        Task.Run(PopulatePreviousScoresBoard);
	}

    private async Task PopulatePreviousScoresBoard()
    {
        await GetPreviousUserScores();

        scoresUpdated = true;
        
    }

    private void Update()
    {
        if(scoresUpdated)
        {
            scoresUpdated = !scoresUpdated;
            UpdatePreviousScores();
        }
    }

    void UpdatePreviousScores()
    {
        foreach (UserRatings userRating in entriesForCurrentUser)
        {
            previousScoresText.text += userRating.Scenario + " | " + userRating.Score + " | " + userRating.TimeInSeconds +  "\n";
        }

        previousScoresTitleText.text +=  " " + GlobalLogin.Instance.loggedInUser;
    }

    private async Task<List<UserRatings>> GetPreviousUserScores()
    {
        var table = AzureMobileServiceClient.Client.GetTable<UserRatings>();

        var allEntries = await table.ToListAsync();

        foreach (var item in allEntries)
        {
            if(item.Username.Equals(GlobalLogin.Instance.loggedInUser))
            {
                entriesForCurrentUser.Add(item);
            }
        }

        return allEntries;
    }


}
