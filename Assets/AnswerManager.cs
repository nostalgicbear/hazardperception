﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerManager : MonoBehaviour {

    public QuestionManager questionManager;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SendAnswerToQuestionManager()
    {
        
        questionManager.ValidateAnswer(this.gameObject);
    }

    
}
