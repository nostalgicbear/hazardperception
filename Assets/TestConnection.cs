﻿using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using System;

public class TestConnection : MonoBehaviour {


    // Use this for initialization
    void Start()
    {
        Task.Run(TestTableConnection);
    }

    private async Task TestTableConnection()
    {
        Debug.Log("Trying to write");
        try
        {


            var table = AzureMobileServiceClient.Client.GetTable<UserLogins>();


            Debug.Log("iNSERTING TEST DATA");
            await TestInsertAsync(table);

            Debug.Log("Reading data");
            await TestToListAsync(table);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    private async Task TestInsertAsync(IMobileServiceTable<UserLogins> table)
    {
        await table.InsertAsync(new UserLogins { Name = "Jay", Password = "chicken"});
    }

    private async Task<List<UserLogins>> TestToListAsync(IMobileServiceTable<UserLogins> table)
    {
        var allEntries = await table.ToListAsync();

        foreach (var item in allEntries)
        {
            Debug.Log(item.Name + " | " + item.Password);
        }

        return allEntries;
    }
}
