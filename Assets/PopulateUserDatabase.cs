﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;

public class PopulateUserDatabase : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Task.Run(GetLoginTable);
	}

    private async Task SendLoginInfoToAzure(IMobileServiceTable<UserLogins> table)
    {
        Debug.Log("Sending login data to azure");
        await table.InsertAsync(new UserLogins { Name = "Ish", Password = GenerateRandomPassword() }
        );

        //After values have been sent to the database, change back to menu
    }


    private async Task GetLoginTable()
    {
        var table = AzureMobileServiceClient.Client.GetTable<UserLogins>();
        Debug.Log(table.TableName);
        Debug.Log("Received Login table from azure");
        await SendLoginInfoToAzure(table);
    }

    private string GenerateRandomPassword()
    {
        List<string> passwords = new List<string>{"password1", "hello", "secret", "office", "friend", "coco"};

        string password = passwords[Random.Range(0, passwords.Count - 1)];
        return password;
    }
}
